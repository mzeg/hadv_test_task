package ru.hoteladvisors.testtask.composer;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.select.annotation.*;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.*;
import ru.hoteladvisors.testtask.bean.ProcessDataBean;
import ru.hoteladvisors.testtask.bean.ProcessData;
import ru.hoteladvisors.testtask.entity.AddressEntity;
import ru.hoteladvisors.testtask.entity.ContactEntity;

import javax.naming.InitialContext;
import javax.naming.NamingException;

@SuppressWarnings("serial")
public class IndexComposer extends GenericForwardComposer<Component> {
    @Wire
    private Listbox contactListbox;
    @Wire
    private Listbox addressListbox;

    @Wire
    private Textbox txLastName;
    @Wire
    private Textbox txFirstName;
    @Wire
    private Textbox txMiddleName;
    @Wire
    private Textbox txPhone;
    @Wire
    private Textbox txPostIndex;
    @Wire
    private Textbox txCity;
    @Wire
    private Textbox txStreet;
    @Wire
    private Textbox txBuilding;
    @Wire
    private Textbox txFlat;

    @Wire
    Panel panelAddContact;
    @Wire
    Panel panelAddAddress;

    private ListModelList<ContactEntity> contactDataModel = new ListModelList<>();
    private ListModelList<AddressEntity> addressDataModel = new ListModelList<>();

    private ProcessData processData; // EJB and Inject annotation don't work in ZK Composer

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        // Lookup processData because of EJB and Inject annotation don't work in ZK Composer
        processData = lookupProcessData();

        reload();

        panelAddContact.setOpen(false);
        panelAddAddress.setOpen(false);
    }

    public void onSelectContact() {
        ContactEntity selected = contactDataModel.get(contactListbox.getSelectedIndex());

        addressDataModel = new ListModelList<>();
        addressDataModel.addAll(processData.findAddressesByContact(selected));

        addressListbox.setModel(addressDataModel);
    }

    public void onDeleteContact(ForwardEvent event) {
        Integer contactId = (Integer) event.getData();
        Messagebox.show(
                "Удалить контакт и все его адреса?",
                "Подрвердите удаление",
                Messagebox.YES | Messagebox.NO,
                Messagebox.QUESTION,
                new EventListener<Event>() {
                    @Override
                    public void onEvent(Event event) throws Exception {
                        if (event.getName().equals(Messagebox.Button.YES.event)) {
                            processData.deleteContact(contactId);
                            reload();
                        }
                    }
                }
        );
    }

    public void onDeleteAddress(ForwardEvent event) {
        Integer addressId = (Integer) event.getData();
        Messagebox.show(
                "Удалить адрес?",
                "Подрвердите удаление",
                Messagebox.YES | Messagebox.NO,
                Messagebox.QUESTION,
                new EventListener<Event>() {
                    @Override
                    public void onEvent(Event event) throws Exception {
                        if (event.getName().equals(Messagebox.Button.YES.event)) {
                            processData.deleteAddress(addressId);
                            onSelectContact();
                        }
                    }
                }
        );
    }

    public void onAddContact() {
        ContactEntity contact = new ContactEntity();
        contact.setLastName(txLastName.getValue());
        contact.setFirstName(txFirstName.getValue());
        contact.setMiddleName(txMiddleName.getValue());
        contact.setPhone(txPhone.getValue());
        if (contact.getLastName().isEmpty() || contact.getFirstName().isEmpty()) {
            Messagebox.show(
                    "Фамилия и имя должны быть указаны",
                    "Ошибка",
                    Messagebox.OK,
                    Messagebox.ERROR
            );
            return;
        }

        processData.saveContact(contact);

        txLastName.setValue("");
        txFirstName.setValue("");
        txMiddleName.setValue("");
        txPhone.setValue("");

        reload();
    }

    public void onAddAddress() {
        if (contactListbox.getSelectedIndex() < 0) {
            Messagebox.show(
                    "Выберите контакт, для которого добавляется адрес",
                    "Ошибка",
                    Messagebox.OK,
                    Messagebox.ERROR
            );
            return;
        }

        ContactEntity selected = contactDataModel.get(contactListbox.getSelectedIndex());

        AddressEntity address = new AddressEntity();
        address.setContact(selected);
        address.setPostIndex(txPostIndex.getValue());
        address.setCity(txCity.getValue());
        address.setStreet(txStreet.getValue());
        address.setBuilding(txBuilding.getValue());
        address.setFlat(txFlat.getValue());
        if (address.getCity().isEmpty() || address.getStreet().isEmpty()) {
            Messagebox.show(
                    "Город и улица должны быть указаны",
                    "Ошибка",
                    Messagebox.OK,
                    Messagebox.ERROR
            );
            return;
        }

        processData.saveAddress(address);

        txPostIndex.setValue("");
        txCity.setValue("");
        txStreet.setValue("");
        txBuilding.setValue("");
        txFlat.setValue("");

        onSelectContact();
    }

    private void reload() {
        contactDataModel = new ListModelList<>();
        contactDataModel.addAll(processData.findAllContacts());
        addressDataModel = new ListModelList<>();

        contactListbox.setModel(contactDataModel);
        addressListbox.setModel(addressDataModel);
    }

    private ProcessData lookupProcessData() throws NamingException {
        String beanName = ProcessDataBean.class.getSimpleName();
        String viewClassName = ProcessData.class.getName();
        return (ProcessData) new InitialContext().lookup("java:global/testtask/testtask-1.0-SNAPSHOT/" + beanName + "!" + viewClassName);
    }
}
