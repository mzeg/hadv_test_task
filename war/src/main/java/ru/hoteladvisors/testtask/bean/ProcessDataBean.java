package ru.hoteladvisors.testtask.bean;

import ru.hoteladvisors.testtask.entity.AddressEntity;
import ru.hoteladvisors.testtask.entity.ContactEntity;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
@Remote(ProcessData.class)
public class ProcessDataBean implements ProcessData {

    @PersistenceContext(unitName = "haUnit")
    private EntityManager entityManager;

    public List<ContactEntity> findAllContacts() {
        return entityManager.createQuery("SELECT c FROM ContactEntity c").getResultList();
    }

    @Override
    public List<AddressEntity> findAddressesByContact(ContactEntity contact) {
        return entityManager.createQuery("SELECT a FROM AddressEntity a WHERE contact=?1")
                .setParameter(1, contact).getResultList();
    }

    @Override
    public void deleteContact(Integer id) {
        ContactEntity contact = entityManager.find(ContactEntity.class, id);
        List<AddressEntity> addressList = findAddressesByContact(contact);
        for (AddressEntity address : addressList) {
            entityManager.remove(address);
        }
        entityManager.remove(contact);
    }

    @Override
    public void deleteAddress(Integer id) {
        AddressEntity address=entityManager.find(AddressEntity.class, id);
        entityManager.remove(address);
    }

    @Override
    public void saveContact(ContactEntity contact) {
        entityManager.persist(contact);
    }

    @Override
    public void saveAddress(AddressEntity address) {
        entityManager.persist(address);
    }
}
