package ru.hoteladvisors.testtask.bean;

import ru.hoteladvisors.testtask.entity.AddressEntity;
import ru.hoteladvisors.testtask.entity.ContactEntity;

import java.util.List;

public interface ProcessData {
    List<ContactEntity> findAllContacts();
    List<AddressEntity> findAddressesByContact(ContactEntity contact);

    void deleteContact(Integer id);
    void deleteAddress(Integer id);

    void saveContact(ContactEntity contact);
    void saveAddress(AddressEntity address);
}
