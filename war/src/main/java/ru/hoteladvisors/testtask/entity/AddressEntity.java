package ru.hoteladvisors.testtask.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.io.Serializable;


@Entity
@Data
public class AddressEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    private ContactEntity contact;

    private String postIndex;

    @NotNull
    private String city;

    @NotNull
    private String street;

    private String building;

    private String flat;
}
